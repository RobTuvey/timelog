import { addDays, endOfDay, startOfDay, startOfWeek, getMinutes, hoursToMinutes, secondsToMinutes, addSeconds, compareAsc, areIntervalsOverlapping, differenceInMinutes, getHours } from "date-fns"
import type { Issue } from "./models";

export function getDayRange(date: Date) {
    return {
        start: startOfDay(date),
        end: endOfDay(date)
    }
}

export function getWeekRange(date: Date) {
    return {
        start: startOfWeek(date),
        end: endOfDay(addDays(startOfWeek(date), 6))
    }
}

export type TimelogItem = {
    title: string;
    top: number;
    left: number;
    width: number;
    height: number;
    timeSpent: string;
}

export function arrangeWorklog(issues: Issue[]): TimelogItem[] {
    const objects = issues.flatMap(issue => issue.fields.worklog.worklogs.map(worklog => ({
        title: issue.key,
        timeSpent: worklog.timeSpent,
        start: new Date(worklog.started),
        end: addSeconds(new Date(worklog.started), worklog.timeSpentSeconds),
        duration: secondsToMinutes(worklog.timeSpentSeconds),
    })));

    return arrangeCalendarObjects(objects);
}

interface CalendarObject {
    start: Date;
    end: Date;
    duration: number;
    title: string;
    timeSpent: string;
}

interface PositionedCalendarObject extends CalendarObject {
    top: number;
    left: number;
    width: number;
    height: number;
}

function getIntervalGroups(items: CalendarObject[]): CalendarObject[][] {
    if (items.length <= 1) {
        return [items];
    }

    const [first, ...rest] = items;
    let group = 0;
    let current: Interval = { ...first };
    let groups: CalendarObject[][] = [[first]];

    for (const item of rest) {
        if (areIntervalsOverlapping(current, item)) {
            if (item.end > current.end) {
                current.end = item.end;
            }
            groups[group].push(item);
        }
        else {
            current = { ...item };
            group++;
            groups.push([item]);
        }
    }

    return groups;
}

function positionGroups(groups: CalendarObject[][]): PositionedCalendarObject[] {
    const positioned: PositionedCalendarObject[] = [];

    for (const group of groups) {
        const width = (1 / group.length) * 100;
        const sorted = group.sort((a, b) => compareAsc(a.start, b.start))

        for (let i = 0; i < sorted.length; i++) {
            const item = sorted[i];
            const hours = (getMinutes(item.start) / 60) + getHours(item.start);
            positioned.push({
                ...item,
                top: hours * 150,
                left: 100 - (width * (i + 1)),
                height: (differenceInMinutes(item.end, item.start) / 60) * 150,
                width
            });
        }
    }

    return positioned;
}

function arrangeCalendarObjects(items: CalendarObject[]): PositionedCalendarObject[] {
    const groups = getIntervalGroups(items);

    return positionGroups(groups);
}
