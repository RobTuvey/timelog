export function clickAway(node: HTMLElement) {
    function handleClick(event: MouseEvent) {
        if (node && !node.contains(event.target as Node)) {
            node.dispatchEvent(new CustomEvent('click_outside'));
        }
    }

    document.addEventListener('click', handleClick, true);

    return {
        destroy() {
            document.removeEventListener('click', handleClick, true);
        }
    }
}
