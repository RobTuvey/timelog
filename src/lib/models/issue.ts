import type { PageResult } from "./pageResult";
import type { Worklog } from "./worklog";

export type Issue = {
    id: string;
    key: string;
    fields: {
        worklog: PageResult<'worklogs', Worklog>;
        summary: string;
        status: {
            name: string;
        }
    }
}