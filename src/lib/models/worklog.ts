export type Worklog = {
    created: string;
    started: string;
    timeSpentSeconds: number;
    timeSpent: string;
    author: {
        accountId: string;
    }
}