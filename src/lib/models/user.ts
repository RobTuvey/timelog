export type User = {
	accountId: string;
	displayName: string;
	emailAddress: string;
	avatarUrls: {
		'48x48': string;
		'32x32': string;
		'16x16': string;
	};
};
