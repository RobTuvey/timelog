export type PageResult<TKey extends string, TItem> = {
    startAt: number;
    maxResults: number;
    total: number;
} & PageResultData<TKey, TItem>;

type PageResultData<TKey extends string, TItem> = {
    [key in TKey]: TItem[];
}