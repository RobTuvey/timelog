export type Resource = {
	id: string;
	url: string;
	name: string;
};
