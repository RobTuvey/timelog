import type { Issue, PageResult } from "$lib/models";
import api from ".";

export function searchIssues(jql: string, fields: string[]) {
    return api.get<PageResult<'issues', Issue>>("api/latest/search", { params: { jql, fields: fields.join(",") } });
}