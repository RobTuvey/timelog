import { goto } from '$app/navigation';
import axios from 'axios';
import Cookies from 'js-cookie';
import { refreshToken } from './refreshToken';

const api = axios.create({});

api.interceptors.response.use(
	(response) => response,
	error => {
		const originalRequest = error?.config;

		if (error.response.status === 401 && !originalRequest.isRetry) {
			originalRequest.isRetry = true;

			const token = Cookies.get('refresh_token');
			if (token) {
				return refreshToken(token)
					.then(response => {
						const bearerToken = `Bearer ${response.data.access_token}`;
						api.defaults.headers.common['Authorization'] = bearerToken;
						originalRequest.headers.Authorization = bearerToken;
						Cookies.set("refresh_token", response.data.refresh_token);

						return api(originalRequest);
					})
			}
		}
		else if (error.response.status === 401) {
			Cookies.remove('refresh_token');
			goto('/login');
		}

		return Promise.reject(error);
	}
);

export default api;

export * from './getMyself';
export * from './getAuthorizeUrl';
export * from './getAccessToken';
export * from './getAccessibleResources';
export * from './getWorkedTickets';
export * from './getIssueWorklog';
export * from './refreshToken';
export * from './getCurrentSprint';
export * from './addWorklog';
export * from './searchByKey';