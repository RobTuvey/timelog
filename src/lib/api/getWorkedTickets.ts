import { searchIssues } from "./searchIssues";
import { getIssueWorklogs } from "./getIssueWorklog";
import { format } from "date-fns";

export function getWorkedTickets(start: Date, end: Date) {
    const jql = `worklogAuthor = currentUser() AND worklogDate is not EMPTY AND worklogDate >= ${format(start, "yyyy-MM-dd")} AND worklogDate <= ${format(end, "yyyy-MM-dd")}`;

    return searchIssues(jql, ["worklog", "summary"])
        .then(getIssueWorklogs);
}

