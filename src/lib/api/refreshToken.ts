import api from '.';
import type { AccessToken } from '$lib/models';
import { PUBLIC_CLIENT_ID, PUBLIC_CLIENT_SECRET } from '$env/static/public';

export function refreshToken(token: string) {
	const data = {
		grant_type: 'refresh_token',
		client_id: PUBLIC_CLIENT_ID,
		client_secret: PUBLIC_CLIENT_SECRET,
		refresh_token: token
	};

	return api.post<AccessToken>('https://auth.atlassian.com/oauth/token', data);
}
