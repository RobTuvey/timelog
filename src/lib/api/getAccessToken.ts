import {
	PUBLIC_CLIENT_ID,
	PUBLIC_CLIENT_SECRET,
	PUBLIC_REDIRECT_URL
} from '$env/static/public';
import type { AccessToken } from '$lib/models';
import api from '.';

export function getAccessToken(code: string) {
	const data = {
		grant_type: 'authorization_code',
		client_id: PUBLIC_CLIENT_ID,
		client_secret: PUBLIC_CLIENT_SECRET,
		code,
		redirect_uri: PUBLIC_REDIRECT_URL
	};

	return api.post<AccessToken>('https://auth.atlassian.com/oauth/token', data);
}
