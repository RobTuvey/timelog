import type { Resource } from '$lib/models';
import api from '.';

export function getAccessibleResources() {
	return api.get<Resource[]>(
		'https://api.atlassian.com/oauth/token/accessible-resources'
	);
}
