import { searchIssues } from "./searchIssues";

export function searchByKey(term: string) {
    const jql = `assignee = currentUser() AND summary ~ "${term}*"`;

    return searchIssues(jql, ["summary"]);
}