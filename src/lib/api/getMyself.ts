import type { User } from '$lib/models';
import api from '.';

export function getMyself() {
	return api.get<User>('api/2/myself').then((response) => response.data);
}
