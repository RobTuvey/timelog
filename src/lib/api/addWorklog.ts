import { format } from "date-fns";
import api from ".";

export function addWorklog(key: string, started: Date, seconds: number) {
    return api.post(`api/3/issue/${key}/worklog`, {
        started: format(started, "yyyy-MM-dd'T'HH:mm:ss.SSSxx"),
        timeSpentSeconds: seconds
    })
}