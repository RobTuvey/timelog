import { PUBLIC_CLIENT_ID, PUBLIC_REDIRECT_URL } from '$env/static/public';
import Cookies from 'js-cookie';

export function getAuthorizeUrl() {
	const params = new URLSearchParams({
		audience: 'api.atlassian.com',
		client_id: PUBLIC_CLIENT_ID,
		scope: 'read:jira-user read:jira-work write:jira-work offline_access',
		redirect_url: PUBLIC_REDIRECT_URL,
		state: Cookies.get('session_id') ?? '',
		response_type: 'code',
		prompt: 'consent'
	});

	return `https://auth.atlassian.com/authorize?${params.toString()}`;
}
