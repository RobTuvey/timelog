import { getIssueWorklogs } from "./getIssueWorklog";
import { searchIssues } from "./searchIssues";

export function getCurrentSprint() {
    const jql = "Sprint in openSprints() and assignee = currentUser()";

    return searchIssues(jql, ["worklog", "summary", "status"])
        .then(getIssueWorklogs);
}