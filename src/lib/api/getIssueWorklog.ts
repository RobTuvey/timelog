import type { Issue, PageResult, Worklog } from "$lib/models";
import type { AxiosResponse } from "axios";
import api from ".";

export async function getIssueWorklogs(
    response: AxiosResponse<PageResult<'issues', Issue>>
) {
    for (const issue of response.data.issues) {
        try {
            const total = issue.fields.worklog.total;
            const maxResults = issue.fields.worklog.maxResults;

            if (total > maxResults) {
                const worklogResponse = await getIssueWorklog(issue.key);

                issue.fields.worklog = worklogResponse.data;
            }
        }
        catch (error) {
            console.error(error);
        }
    }

    return response;
}

export function getIssueWorklog(key: string) {
    return api.get<PageResult<'worklogs', Worklog>>(`api/2/issue/${key}/worklog`);
}