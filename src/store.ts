import type { User } from '$lib/models';
import { writable } from 'svelte/store';

export const session = writable<{ sessionId?: string; user?: User }>({});
