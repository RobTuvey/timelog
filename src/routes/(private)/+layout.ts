import api, {
	getAccessToken,
	getAccessibleResources,
	getMyself,
	refreshToken
} from '$lib/api';
import { redirect } from '@sveltejs/kit';
import Cookies from 'js-cookie';
import { v4 as guid } from 'uuid';

export const prerender = true;
export const ssr = false;

export async function load() {
	const sessionId = getSessionId();
	await handleRedirect();

	const token = Cookies.get('refresh_token');

	if (!token) {
		throw redirect(302, '/login');
	}

	await setupApi(token);

	const user = await getMyself();

	return { sessionId, user };
}

function getSessionId() {
	let sessionId = Cookies.get('session_id');

	if (sessionId) {
		return sessionId;
	} else {
		sessionId = guid();
		Cookies.set('session_id', sessionId);

		return sessionId;
	}
}

async function handleRedirect() {
	const params = new URLSearchParams(window.location.search);

	const code = params.get('code');
	if (code) {
		const result = await getAccessToken(code);

		Cookies.set('refresh_token', result.data.refresh_token, { expires: 365 });

		window.location.search = '';
	}
}

async function getUrl() {
	const resource = await getAccessibleResources();

	if (resource.data.length > 0) {
		const currentResource = resource.data[0];

		if (currentResource) {
			api.defaults.baseURL = `https://api.atlassian.com/ex/jira/${currentResource.id}/rest`;
		}
	} else {
		console.error('No available resources');
	}
}

async function setupApi(token: string) {
	const result = await refreshToken(token);

	Cookies.set('refresh_token', result.data.refresh_token, { expires: 365 });

	api.defaults.headers.common[
		'Authorization'
	] = `Bearer ${result.data.access_token}`;

	await getUrl();
}
